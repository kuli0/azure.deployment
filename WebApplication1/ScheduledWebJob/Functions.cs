﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Microsoft.Azure.WebJobs;
using Newtonsoft.Json;
using RestSharp;

namespace ScheduledWebJob
{
    public class Functions
    {
        // This function will be triggered based on the schedule you have set for this WebJob
        // This function will enqueue a message on an Azure Queue called queue
        [NoAutomaticTrigger]
        public static void ManualTrigger()
        {
            try
            {
                var apiUrl = ConfigurationManager.AppSettings["apiUrl"];
                var client = new RestClient(apiUrl);
                var req = new RestRequest("/api/times", Method.PUT);
                req.RequestFormat = DataFormat.Json;
                req.AddBody(new TimeValue
                {
                    Time = DateTime.Now,
                    Source = "scheduled web job"
                });
                var response = client.Execute(req);
                Log($"Response status is {response.StatusCode}");
                Log($"Response body is {response.Content}");
            }
            catch (Exception e)
            {
                Log($"Error: {e.Message}");
            }
        }

        private static void Log(string msg)
        {
            Console.Write(msg);
            Console.WriteLine();
        }
    }
}
