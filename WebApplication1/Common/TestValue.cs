﻿using System;
using System.Collections.Generic;

namespace Common
{
    public class TestValue
    {
        private static readonly IDictionary<Guid, TestValue> Values = new Dictionary<Guid, TestValue>();

        public Guid Id { get; set; }
        public string Value { get; set; }

        public static IEnumerable<TestValue> ListAll()
        {
            return Values.Values;
        }

        public static TestValue GetById(Guid id)
        {
            if (Values.ContainsKey(id))
            {
                return Values[id];
            }

            return null;
        }

        public static void Create()
        {
            var value = new TestValue
            {
                Id = Guid.NewGuid(),
                Value = "created..."
            };
            Values[value.Id] = value;
        }

        public static void Update(Guid id, string value)
        {
            Values[id].Value = value;
        }
    }
}