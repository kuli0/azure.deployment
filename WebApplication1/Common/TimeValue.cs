﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Common
{
    public class TimeValue
    {
        public DateTime Time { get; set; }
        public string Source { get; set; }

        private static readonly List<TimeValue> times = new List<TimeValue>();

        public static IEnumerable<TimeValue> GetAll()
        {
            return times;
        }

        public static void Add(TimeValue time)
        {
            times.Add(time);
        }
    }
}