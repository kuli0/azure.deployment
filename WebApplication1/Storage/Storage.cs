﻿using System;
using System.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Microsoft.WindowsAzure.Storage.RetryPolicies;

namespace Storage
{
    public class Storage
    {
        private static Lazy<CloudStorageAccount> Account = new Lazy<CloudStorageAccount>(() =>
        {
            return CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["AzureWebJobsStorage"].ToString());
        });

        public static CloudQueue GetQueue(string name)
        {
            var client = Account.Value.CreateCloudQueueClient();
            client.DefaultRequestOptions.RetryPolicy = new LinearRetry(TimeSpan.FromSeconds(3), 3);
            var queue = client.GetQueueReference(name);
            queue.CreateIfNotExists();
            return queue;
        }
    }
}