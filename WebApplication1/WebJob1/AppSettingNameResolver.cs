using System.Configuration;
using System.Linq;
using Microsoft.Azure.WebJobs;

namespace WebJob1
{
    internal class AppSettingNameResolver : INameResolver
    {
        public string Resolve(string name)
        {
            if (ConfigurationManager.AppSettings.AllKeys.Contains(name))
            {
                return ConfigurationManager.AppSettings[name];
            }

            return null;
        }
    }
}