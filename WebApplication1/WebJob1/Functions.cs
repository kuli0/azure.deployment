﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Timers;
using Newtonsoft.Json;
using RestSharp;

namespace WebJob1
{
    public class Functions
    {
        public static void ProcessTestValue([QueueTrigger("kjuu")] TestValue value)
        {
            try
            {
                var apiUrl = ConfigurationManager.AppSettings["apiUrl"];
                Log($"Received message: {JsonConvert.SerializeObject(value)}");
                Log($"Url is {apiUrl}");
                var client = new RestClient(apiUrl);
                value.Value = "nagyon processed.";
                var req = new RestRequest("/api/testvalues", Method.POST);
                req.RequestFormat = DataFormat.Json;
                req.AddBody(value);
                var response = client.Execute(req);
                Log($"Response status is {response.StatusCode}");
                Log($"Response body is {response.Content}");
            }
            catch (Exception e)
            {
                Log($"Error: {e.Message}");
            }
        }

        //public static async Task AddTime([TimerTrigger("%triggerInterval%", RunOnStartup = true)] TimerInfo timerInfo)
        //{
        //    try
        //    {
        //        await Task.Delay(40000);
        //        var apiUrl = ConfigurationManager.AppSettings["apiUrl"];
        //        var client = new RestClient(apiUrl);
        //        var req = new RestRequest("/api/times", Method.PUT);
        //        req.RequestFormat = DataFormat.Json;
        //        req.AddBody(new TimeValue
        //        {
        //            Time = DateTime.Now,
        //            Source = "triggered web job 1"
        //        });
        //        var response = client.Execute(req);
        //        Log($"Response status is {response.StatusCode}");
        //        Log($"Response body is {response.Content}");
        //    }
        //    catch (Exception e)
        //    {
        //        Log($"Error: {e.Message}");
        //    }
        //}

        //public static async Task AddTime2([TimerTrigger("%triggerInterval2%", RunOnStartup = true)] TimerInfo timerInfo)
        //{
        //    try
        //    {
        //        await Task.Delay(40000);
        //        var apiUrl = ConfigurationManager.AppSettings["apiUrl"];
        //        var client = new RestClient(apiUrl);
        //        var req = new RestRequest("/api/times", Method.PUT);
        //        req.RequestFormat = DataFormat.Json;
        //        req.AddBody(new TimeValue
        //        {
        //            Time = DateTime.Now,
        //            Source = "triggered web job 2"
        //        });
        //        var response = client.Execute(req);
        //        Log($"Response status is {response.StatusCode}");
        //        Log($"Response body is {response.Content}");
        //    }
        //    catch (Exception e)
        //    {
        //        Log($"Error: {e.Message}");
        //    }
        //}

        public static void ErrorTest([QueueTrigger("errorteszt")] string msg)
        {
            if (msg == "error")
            {
                throw new Exception();
            }
        }

        private static void Log(string msg)
        {
            Console.Write(msg);
            Console.WriteLine();
        }
    }
}
