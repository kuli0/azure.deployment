﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Common;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class TestValuesController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<TestValue> Get()
        {
            return TestValue.ListAll();
        }

        // GET api/<controller>/5
        public void Get(Guid id)
        {
            var value = TestValue.GetById(id);
            value.Value = "processing...";
            var queue = Storage.Storage.GetQueue("kjuu");
            var msg = new CloudQueueMessage(JsonConvert.SerializeObject(value));
            queue.AddMessage(msg);
        }

        // POST api/<controller>
        public void Post(TestValue value)
        {
            var storedValue = TestValue.GetById(value.Id);
            if (storedValue != null)
            {
                storedValue.Value = value.Value;
            }
        }

        // PUT api/<controller>
        public void Put()
        {
            TestValue.Create();
        }
    }
}