﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;

namespace WebApplication1.Controllers
{
    public class ErrorTestController : ApiController
    {
        // GET: api/ErrorTest
        public void Get()
        {
            var random = new Random();
            var messages = new[] {"error", "nem error"};
            var queue = Storage.Storage.GetQueue("errorteszt");
            for (int i = 0; i < 15; i++)
            {
                var msg = new CloudQueueMessage(messages[random.Next(1)]);
                queue.AddMessage(msg);
            }
        }
    }
}
