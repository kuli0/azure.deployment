﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Common;

namespace WebApplication1.Controllers
{
    public class TimesController : ApiController
    {
        // GET: api/Times
        public IEnumerable<TimeValue> Get()
        {
            return TimeValue.GetAll();
        }

        // PUT: api/Times/5
        public void Put(TimeValue time)
        {
            TimeValue.Add(time);
        }
    }
}